module scan_dat
   !
   use iotk_module
   !
   implicit none
   !
   contains
      !
      !
      subroutine gettag(iunit, name, size, columns, type)
         !
         implicit none
         !
         integer, intent(in)              :: iunit 
         integer, intent(out)             :: size
         integer, intent(out)             :: columns
         character(len=256), intent(in)   :: name 
         character(len=256), intent(out)  :: type 
         !
         !Temp variables
         integer                          :: iunit_, ierr
         character(len=256)               :: name_
         character(len=256)               :: attr
         logical                          :: found
         !
         iunit_ = iunit
         name_ = name
         !
         size = 0
         !
         call iotk_scan_begin(unit=iunit_, name=name_, attr=attr, found=found, ierr=ierr)
         if (found) then
            call iotk_scan_attr(attr=attr, name='size', val=size, found=found, ierr=ierr)
            call iotk_scan_attr(attr=attr, name='columns', val=columns, found=found, ierr=ierr)
            call iotk_scan_attr(attr=attr, name='type', val=type, found=found, ierr=ierr)
         endif
         call iotk_scan_end(unit=iunit_, name=name_, ierr=ierr)
         !
      end subroutine gettag
      !
      subroutine read_integer(iunit, name, dat, size)
         !
         implicit none
         !
         integer, intent(in)              :: iunit 
         character(len=256), intent(in)   :: name 
         !
         integer, intent(in)              :: size
         integer, intent(out)             :: dat(size)
         !
         !Temporary Variables 
         real, allocatable                :: dat_(:)
         integer                          :: iunit_, ierr
         character(len=256)               :: name_
         character(len=256)               :: attr
         logical                          :: found
         !
         iunit_ = iunit
         name_ = name
         !
         dat = 0
         !
         call iotk_scan_dat(unit=iunit_, name=name_, dat=dat, found=found, ierr=ierr)
         !
      end subroutine read_integer
      !
      subroutine read_real(iunit, name, dat, size)
         !
         implicit none
         !
         integer, intent(in)              :: iunit 
         character(len=256), intent(in)   :: name 
         !
         integer, intent(in)              :: size
         real, intent(out)                :: dat(size)
         !
         !Temporary Variables 
         real, allocatable                :: dat_(:)
         integer                          :: iunit_, ierr
         character(len=256)               :: name_
         character(len=256)               :: attr
         logical                          :: found
         !
         iunit_ = iunit
         name_ = name
         !
         dat = 0.0
         !
         call iotk_scan_dat(unit=iunit_, name=name_, dat=dat, found=found, ierr=ierr)
         !
      end subroutine read_real
      !
      subroutine read_complex(iunit, name, dat, size)
         !
         implicit none
         !
         integer, intent(in)              :: iunit 
         character(len=256), intent(in)   :: name 
         !
         integer, intent(in)              :: size
         complex, intent(out)             :: dat(size)
         !
         !Temporary Variables 
         complex, allocatable             :: dat_(:)
         integer                          :: iunit_, ierr
         character(len=256)               :: name_
         character(len=256)               :: attr
         logical                          :: found
         !
         iunit_ = iunit
         name_ = name
         !
         dat = 0.0
         !
         call iotk_scan_dat(unit=iunit_, name=name_, dat=dat, found=found, ierr=ierr)
         !
      end subroutine read_complex
      !
end module scan_dat
