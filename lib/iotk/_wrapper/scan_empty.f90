subroutine scan_empty(iunit, name, attr, found, ierr)
   !
   use iotk_module
   !
   implicit none
   !
   integer,                   intent(in)  :: iunit
   character(len=256),        intent(in)  :: name 
   character(len=256),        intent(out) :: attr 
   logical,                   intent(out) :: found
   integer,                   intent(out) :: ierr
   ! 
   !Temp Input arrays
   integer                    :: iunit_
   character(len=256)         :: name_
   !
   !Temp Output arrays
   character(len=256)         :: attr_
   logical                    :: found_
   integer                    :: ierr_
   !
   !Assign Input defaults
   iunit_ = iunit
   name_ = name
   !
   !Call Routine
   call iotk_scan_empty(unit=iunit, name=trim(name_), attr=attr_, found=found_, ierr=ierr_)
   !
   !Assign Output varables
   attr = attr_
   found = found_
   ierr = ierr_
   !
end subroutine
