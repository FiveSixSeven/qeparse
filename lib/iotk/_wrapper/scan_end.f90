subroutine scan_end(iunit, name, ierr)
   !
   use iotk_module
   !
   implicit none
   !
   integer,             intent(in)  :: iunit
   character(len=256),  intent(in)  :: name 
   integer,             intent(out) :: ierr
   ! 
   !Temp Input arrays
   integer                    :: iunit_
   character(len=256)         :: name_
   !
   !Temp Output arrays
   integer                    :: ierr_
   !
   !Assign Input defaults
   iunit_ = iunit
   name_ = name
   !
   !Call Routine
   call iotk_scan_end(unit=iunit_, name=trim(name_), ierr=ierr_)
   !
   !Assign Output varables
   ierr = ierr_
   !
end subroutine
