subroutine close_read(iunit, ierr)
   !
   use iotk_module
   !
   implicit none
   !
   !Main IN/OUT Variables
   integer,          intent(in)              :: iunit
   integer,          intent(out)             :: ierr
   !
   !Temp ariables
   integer              :: iunit_
   integer              :: ierr_
   !
   !Assign Input defaults
   iunit_ = iunit
   !
   !Call Routine
   call iotk_close_read(iunit_, ierr=ierr_)
   !
   !Assign Output variables
   ierr = ierr_
   !
end subroutine close_read
