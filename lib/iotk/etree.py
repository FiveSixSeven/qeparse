import re
import os
import ast
import numpy as np
from lxml import etree
from .classes import readIOTK

def parse(source, base_url=None):
    return etree.parse(source, parser=_getparser(), base_url=base_url)

def fromstring(source, base_url=None):
    return etree.fromstring(source, parser=_getparser(), base_url=base_url)

def _getparser():
    """ Add the modified Element class _QETree to the lxml parser"""
    lookup = etree.ElementDefaultClassLookup(element=_QETree)
    parser = etree.XMLParser()
    parser.set_element_class_lookup(lookup)
    return parser

class _QETree(etree.ElementBase):
    # Modified Element Class for lxml.etree, add to the parser with
    # _getparser(). Provides Element with two additional methods
    # for parsing iotk files: Element.get_iotk_data(), and
    # Element.get_iotk_link()

    def get_iotk_data(self):
        """
        Converts self.text that compiles with the QUANTUM-ESPRESSO IOTK
        definitions of data into the correct type and form.

        Format:

        <name type=TYPE size=NUM [columns=NCOLS] >
        ...
        text
        ...
        <name/>

        Where:
          TYPE: Is the data type (integer, real, complex, character, or logical)
          NUM: Total size of the data to be read (NOT numpy.shape!)
          NCOLS: (optional) Number of columns that the data is distributed amongst

        @return Converted numerical, boolean, string data (or None)
        @defreturn Varies (dependent on self.attrib)
        """

        # Check Attributes
        try:
            type = self.attrib['type']
            size = int(self.attrib['size'])
            columns = int(self.attrib.get('columns', 1))
        except KeyError:
            return None

        # Shape the data
        if columns > 1:
            shape = [columns, size / columns]
        else:
            shape = size

        # Convert the types
        if type in ('real', 'integer'):
            retdata = self.text.split()
            retdata = np.array([ast.literal_eval(i) for i in retdata])
            retdata = retdata.reshape(shape, order='C')
            if size > 1:
                return retdata
            else:
                # Only a one-element array
                return retdata[0]
        elif type == 'logical':
            if self.text.upper() == 'T':
                return True
            else:
                return False
        elif type == 'character':
            return self.text.strip()
        else:
            raise ParseError('Unknown type found: {}'.format(type))

    def get_iotk_link(self, savedir=None):
        '''
        Open an iotk link
        If binary:  Return IOTKread object. Because of the non-standard
                    binary format, this object will have to be parsed
                    the method defined in iotk.classes.py.
        If not binary:  Return the root lxml.etree (_QETree) element

        Parameters:
            savedir: - The directory of the data-file.xml. This is
                       required because link are stored RELATIVE to
                       the data-file.xml
        '''
        # Number of lines to find if it is a binary file
        preview_iotk_lines = 6

        if 'iotk_link' not in self.attrib.keys():
            return None
        else:
            # Construct the filename, may not be in the correct location
            filepath = os.path.join(self.attrib['iotk_link'])
            if savedir:
                filepath = os.path.join(savedir, filepath)

            # Read the first init_readlines to determine if it is a binary file
            binary = False
            p = re.compile(r"\<\?iotk binary=['\"]([tfTF])['\"]\?\>")
            with open(filepath, 'rb') as f:
                for i in range(preview_iotk_lines):
                    try:
                        line = f.readline()
                        line = line.decode('ascii')
                        m = p.search(line)
                        if m:
                            fboolean = m.group(1)
                            if fboolean.lower() == 't':
                                binary = True
                    except UnicodeDecodeError:
                        pass
            if binary:
                return readIOTK(filepath, binary=True)
            else:
                # Return a _QEetree element
                return parse(filepath).getroot()

class ParseError(SyntaxError):
    pass
