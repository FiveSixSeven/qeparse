#!/usr/bin/env python

distance_to_meters = {
    'Angstrom': 1.0E-10,
    'Bohr': 5.2917721092E-11
}

energy_to_joules = {}
