#! /usr/bin/env python

from __future__ import print_function
import re
import os
import collections
import numpy as np
from .iotk import etree
from .physics import atoms

# =---------------------------------------------------------------------------=#
# Base Class
# =---------------------------------------------------------------------------=#
class _DataFile(object):
    """
    Base class for Quantum-Espresso data files

    Data is stored in the general format:
        record separator1
        ...
        record separatorN
        Data Line 1
        ...
        Data Line M

        Where only one record separator is required
    """

    sizelimit = 100.0 # MB

    # File Extension (defined in subclasses)
    _ext = None

    # Record Separator list, lines split into a list (defined in subclasses)
    _record_sep = None

    def __init__(self, filename, skip=True):
        self.__filename = filename
        self._skip = skip

        # lines and data internal (saved) lists
        # Only populated if called-on, and size is under sizelimit
        self.__lines = []
        self.__data = collections.OrderedDict()

        # Check that the files exists
        if not os.path.isfile(self.filename):
            raise OSError("File Not Found: {}".format(filename))

        # Check that it has the correct extension (subclass)
        temp, ext = os.path.splitext(self.filename)
        if ext != self._ext:
            raise DataFileError('Class {} expects an extension of {}, found {}'.format(
                self.__class__.__name__, self.__class__._ext, ext))

        # Create array of re pattern objects for each line in self.record_sep
        self._patterns = []
        for string in self._record_sep:
            self._patterns.append(re.compile(string))

    @property
    def filename(self):
        '''
        Name of the data file, should not be changed
        '''
        return self.__filename

    @property
    def filesize(self):
        '''
        Size of the data file in MB
        '''
        statinfo = os.stat(self.filename)
        size = statinfo.st_size / 10.0 ** 6
        return size

    def _check_size_ok(self):
        '''
        Check the size against the class variable sizelimit
        '''
        if self.filesize > self.__class__.sizelimit:
            raise FileToLargeError(self.filename)

    @property
    def lines(self):
        '''
            Read all of the data file into a list of strings: self.lines
            skip = True will remove all blank and commented lines
        '''
        self._check_size_ok()
        if not self.__lines:
            with open(self.filename) as f:
                if self._skip:
                    self.__lines = [line.strip() for line in f if not re.search('(^\s*$|^#+)', line)]
                else:
                    self.__lines = [line.strip() for line in f]
        return self.__lines

    @property
    def data(self):
        '''
        Read in a data from file (only under sizelimit) storing value according
        to self._parse_record and package them according to self._return_data
        '''
        if not self.__data:
            self._check_size_ok()
            self._ncount = 1
            with open(self.filename, 'r') as fobj:
                while True:
                    try:
                        # Read through record separator lines in fobj
                        retgroups = self._parse_seperator(fobj)
                        # Read through all data in fobj using _get_record_line
                        # until the next record separator (first line) is reached
                        # (at which point we rewind back one line)
                        self.__data.update(self._return_data(fobj, retgroups))
                    except EOFError:
                        break
        return self.__data

    def __getitem__(self, key):
        return self.data.__getitem__(key)

    def loop(self):
        '''
        Create a generator that will loop through the file and return values according
        to self._parse_record and package them according to self._return_loop
        '''
        self._ncount = 1
        with open(self.filename, 'r') as fobj:
            while True:
                try:
                    # Read through record separator lines in fobj
                    retgroups = self._parse_seperator(fobj)
                    # Read through all data in fobj using _get_record_line
                    # until the next record separator (first line) is reached
                    # (at which point we rewind back one line)
                    yield self._return_loop(fobj, retgroups)
                except EOFError:
                    break

    def __iter__(self):
        return self.loop()

    def _parse_seperator(self, fobj):
        '''
        Perform first read of the fobj, Check that all lines in the
        self._record_sep are found and return any capture groups from results

        Raise an exception if end-of-file (EOFError) or incorrect format (IncorrectFormatError)
        '''
        groups = []
        # Check each of the lines specified in self._patterns
        # (from the list in self.record_sep) is found
        for index, pattern in enumerate(self._patterns):
            line = fobj.readline()
            # raise different errors depending on number
            if not line:
                if index == 0:
                    # Normal end of file
                    raise EOFError
                else:
                    raise IncorrectFormatError(
                        'End of file during record in {}'.format(self.filename)
                    )
            else:
                self._ncount += 1

            match = pattern.search(line)
            if match:
                groups.append(match.groups())
            else:
                raise IncorrectFormatError(
                    self.filename,
                    'Missing record at line number {}'.format(self._ncount)
                )
        return groups

    def _return_data(self, fobj, retgroup):
        '''
        Package the returned record from self._parse_record for self.data
        makes use of the retgroups of of self._parse_seperator

        Assigned in subclass needs to call self._parse_record

        General Format:
            step = retgroups[0][0]
            return dict( int(step) =  self._parse_record(fobj))
        '''
        raise NotImplementedError

    def _return_loop(self, fobj, retgroup):
        '''
        Package the returned record from self._parse_record for self.loop
        makes use of the retgroups of of self._parse_seperator

        Assigned in subclass needs to call self._parse_record

        General Format:
            step = retgroups[0][0]
            time = retgroups[0][1]
            return step, time, self._parse_record(fobj)
        '''
        raise NotImplementedError

    def _parse_record(self, fobj):
        '''
        How the data within a record is to be handled

        To be modified in SubClass

        *SHOULD* use self._get_record_line to read next line as it will back
        up once it finds the beginning of the next record generator

        NOTE:
        We could just return the data in this record to another parser
        but because these files a potentially very long we would
        essentially be reading the file twice

        General Format:
        revalue = [] #or {}
        line = self._get_record_line(fobj)
        while line:
            self._ncount += 1
            #....
            #Parse Record, update return_value (Completed in a SubClass)
            #....
            line = self._get_record_line(fobj)
        return return_value
        '''
        raise NotImplementedError

    def _get_record_line(self, fobj):
        '''
        Read the next line in the RECORD
        Check to see if file is currently at the *first* line of
        RECORD SEPARATOR self._record_sep

        If it IS:
            return the line

        If it IS NOT:
            return None
            and rewind to the fobj  previous position
        '''
        pos_in_file = fobj.tell()
        line = fobj.readline().strip()
        if re.search(self._patterns[0], line):
            fobj.seek(pos_in_file)
            return None
        else:
            return line

# =---------------------------------------------------------------------------=#


# =---------------------------------------------------------------------------=#
# Secondary Base Classes Based on Record Separator
# =---------------------------------------------------------------------------=#
class _StepTimeRecSep(_DataFile):
    '''
    Subclass for files that are separated by

    step1 time1
    ...
    Records
    ...
    stepN timeN
    ...
    Records
    ...

    Examples: *.pos, *vel, *for, *cel, *spr
    '''

    # Two capture groups (step) (time)
    _record_sep = [r'^\s*([^\s]*)\s+([^\s]*)\s*$']

    def __init__(self, filename):
        super().__init__(filename)

    def _return_loop(self, fobj, retgroups):
        step = retgroups[0][0]
        time = retgroups[0][1]
        return int(step), float(time), self._parse_record(fobj)

    def _return_data(self, fobj, retgroups):
        step = retgroups[0][0]
        return {int(step) :  self._parse_record(fobj)}

class _EigenRecSep(_DataFile):
    '''
    Subclass for files that are separated by

    STEP:      step       time
    Eigenvalues (eV), kp =   kpt , spin =  spin
    '''

    _record_sep = [r'^\s*STEP:\s+([^\s]*)\s+([^\s]*)$',
        r'^\s*Eigenvalues\s*\(eV\),\s+kp\s+=\s*([^\s]*)\s*,\s+spin\s+=\s*([^\s]*)\s*$']

    def __init__(self, filename):
        super().__init__(filename)

    def _return_loop(self, fobj, retgroups):
        step = retgroups[0][0]
        time = retgroups[0][1]
        kpt = retgroups[1][0]
        spin = retgroups[1][1]
        return int(step), float(time), int(kpt), int(spin), self._parse_record(fobj)

    def _return_data(self, fobj, retgroups):
        step = retgroups[0][0]
        kpt = retgroups[1][0]
        spin = retgroups[1][1]
        return {int(step):  {int(kpt): {int(spin): self._parse_record(fobj)}}}

# =---------------------------------------------------------------------------=#
#  Third level Base Classes Based on File Types
# =---------------------------------------------------------------------------=#

class _Atomic_XYZ_Files(_StepTimeRecSep):
    """
    Class for Quantum-Espresso Atomic-Based files with values for each xyz dimension.
    Atomic-Based files are those that record values for each atom, as opposed to
    State-Based files with record values for each state.

    Based off of a _StepTimeRecSep files (step time in the record separator)

        Input:
            - filename
                Note: QUANTUM-ESPRESSO always outputs positions in Bohr
            - datafile = (optional) path to the data-file.xml associated with this
                         Atomic-Based file
            - species = (optional) a tuple-of-tuple of atom names (or numbers) and
                        their corresponding totals. Ex: (('O', 32), ('H', 64))
                IMPORTANT: Atoms should be entered in the order they were originally entered
                in the QUANTUM-ESPRESSO input file. This programs makes use of OrderDicts so
                that the order is preserved

        Important: Either datafile or species must be specified, but not both. An Exception
                   will be raised  otherwise.

        Note: QUANTUM-ESPRESSO always outputs positions in Bohr

        Assumed Format:

            step1 time1
            Atom1-XCoordinate  Atom1-YCoordinate  Atom1-ZCoordinate
            ...
            AtomN-XCoordinate  AtomN-YCoordinate  AtomN-ZCoordinate
            step2 time2
            Atom1-XCoordinate  Atom1-YCoordinate  Atom1-ZCoordinate
            ...
            AtomN-XCoordinate  AtomN-YCoordinate  AtomN-ZCoordinate
            .
            .
            .
            stepM timeM
            Atom1-XCoordinate  Atom1-YCoordinate  Atom1-ZCoordinate
            ...
            AtomN-XCoordinate  AtomN-YCoordinate  AtomN-ZCoordinate

        Examples: *.pos, *vel, *for, *cel, *spr

    """

    # Return class for the _parse_record file
    _physics_class = None

    def __init__(self, filename, datafile=None, **species):
        super().__init__(filename)

        # Check the inputs
        if not (species or datafile):
            raise DataFileError('Either the datafile or tuple of the species must be provided with '
                                'a Atomic listed File')
        if (species and datafile):
            raise DataFileError('Both a datafile and a tuple of the species cannot to be used '
                                'simultaneously with a Atomic listed file')

        # Species Info
        self.species = collections.OrderedDict()

        # From initializer
        if species:
            for name, val in species.items():
                self.species[name] = int(val)
        # From Data file
        elif datafile:
            self.datafile = datafile
            self._read_datafile()
        else:
            raise DataFileError('Either a data-file.xml for species dict need to be specified')

    def _read_datafile(self):
        tree = etree.QEetree.parse(self.datafile)
        root = tree.getroot()
        ions = root.find('IONS')
        natoms = ions.find("NUMBER_OF_ATOMS").data
        nspecies = ions.find("NUMBER_OF_SPECIES").data
        # Number of Species
        for num in range(nspecies):
            specie = ions.find('SPECIE.{}'.format(num + 1))
            self.species.update({specie.find('ATOM_TYPE').data: 0})
        # Atom Assignment
        for num in range(natoms):
            atom = ions.find('ATOM.{}'.format(num + 1))
            self.species[atom.attrib['SPECIES'].strip()] += 1

    def _parse_record(self, fobj):
        # set up main data structure
        config_data = collections.OrderedDict()
        for name in self.species:
            config_data.update({name : []})

        # current atom label
        atom_names = list(self.species.keys())
        current_atom = atom_names.pop(0)

        # Loop through the record
        line = self._get_record_line(fobj)
        while line:
            self._ncount += 1
            split_line = line.strip().split()

            # Append current configuration data
            config_data[current_atom].append([float(i) for i in split_line])

            # Check the number of atoms of current_atom, return or go to next atom
            if len(config_data[current_atom]) == self.species[current_atom]:
                # Check to make sure that all species have correct atoms
                if len(atom_names) < 1:
                    for name in config_data:
                        if len(config_data[name]) != self.species[name]:
                            raise IncorrectFormatError(self.filename,
                                    "Number of Read-In atoms and species totals do not match")
                else:
                    # Next Atom label
                    current_atom = atom_names.pop(0)
            line = self._get_record_line(fobj)
        return self.__class__._physics_class(**config_data)

class _Cell_XYZ_Files(_StepTimeRecSep):
    """
    Class for Quantum-Espresso Cell-based files with values for each xyz dimension.
    Cell-Based files contain records 3 unit vectors for each step.

        Input:
            - filename
                Note: QUANTUM-ESPRESSO always outputs positions in Bohr

        Note: QUANTUM-ESPRESSO always outputs positions in Bohr

        Assumed Format:

            step1 time1
            Cell1-XCoordinate  Cell1-YCoordinate  Cell1-ZCoordinate
            Cell2-XCoordinate  Cell2-YCoordinate  Cell2-ZCoordinate
            Cell3-XCoordinate  Cell3-YCoordinate  Cell3-ZCoordinate
            step2 time2
            Cell1-XCoordinate  Cell1-YCoordinate  Cell1-ZCoordinate
            Cell2-XCoordinate  Cell2-YCoordinate  Cell2-ZCoordinate
            Cell3-XCoordinate  Cell3-YCoordinate  Cell3-ZCoordinate
            .
            .
            .
            stepM timeM
            Cell1-XCoordinate  Cell1-YCoordinate  Cell1-ZCoordinate
            Cell2-XCoordinate  Cell2-YCoordinate  Cell2-ZCoordinate
            Cell3-XCoordinate  Cell3-YCoordinate  Cell3-ZCoordinate
    """

    def __init__(self, filename):
        super().__init__(filename)

    def _parse_record(self, fobj):
        # set up main data structure
        config_data = list()

        # Loop through the record
        line = self._get_record_line(fobj)
        while line:
            self._ncount += 1
            split_line = line.strip().split()

            config_data.append([float(i) for i in split_line])

            line = self._get_record_line(fobj)
        return np.array(config_data)


class _State_val_Files(_StepTimeRecSep):
    def __init__(self, filename):
        super().__init__(filename)


class _Eigen_val_Files(_EigenRecSep):
    def __init__(self, filename):
        super().__init__(filename)

    def _parse_record(self, fobj):
        # set up main data structure
        config_data = list()

        # Loop through the record
        line = self._get_record_line(fobj)
        while line:
            self._ncount += 1
            split_line = line.strip().split()

            config_data.extend([float(i) for i in split_line])

            line = self._get_record_line(fobj)
        return np.array(config_data)
# =---------------------------------------------------------------------------=#


# =---------------------------------------------------------------------------=#
#  Public API Classes
# =---------------------------------------------------------------------------=#
class PosFile(_Atomic_XYZ_Files):
    # Position  Extension (Check by Base Class File)
    _ext = '.pos'
    _physics_class = atoms.AtomPositions

    def __init__(self, filename, datafile=None, units='Bohr', **species):
        super().__init__(filename, datafile, **species)

class VelFile(_Atomic_XYZ_Files):
    # Velocity  Extension (Check by Base Class File)
    _ext = '.vel'
    _physics_class = atoms.AtomVelocities

    def __init__(self, filename, datafile=None, species=None, units='AU'):
        super().__init__(filename, datafile, species)

class CelFile(_Cell_XYZ_Files):
    # Cell  Extension (Check by Base Class File)
    _ext = '.cel'

    def __init__(self, filename, units='Bohr'):
        _Cell_XYZ_Files.__init__(self, filename)

        # Position Units
        self.units = units
        if self.units == 'Bohr':
            self._convert = 1.0
        elif self.units == 'Angstrom':
            self._convert = 0.52917721092
        else:
            raise DataFileError("Units for the Cell File can only be Bohr or Angstrom")

class ForFile(_Atomic_XYZ_Files):
    # Force  Extension (Check by Base Class File)
    _ext = '.for'

    def __init__(self, filename, datafile=None, species=None, units='AU'):

        # Velocity  Units
        self.units = units
        if self.units == 'AU':
            self._convert = 1.0
        else:
            raise DataFileError("Units for the Force File can only be in AU")

        super().__init__(filename, datafile, species)

class EigFile(_Eigen_val_Files):
    # Force  Extension (Check by Base Class File)
    _ext = '.eig'

    def __init__(self, filename, units='AU'):

        # Velocity  Units
        self.units = units
        if self.units == 'AU':
            self._convert = 1.0
        else:
            raise DataFileError("Units for the Eigen File can only be in AU")

        super().__init__(filename)
# =---------------------------------------------------------------------------=#

# =---------------------------------------------------------------------------=#
#  Exceptions Classes
# =---------------------------------------------------------------------------=#
class DataFileError(Exception):
    pass


class FileToLargeError(Exception):
    def __init__(self, obj):
        msg = "Error: file '{}' is larger then the {} MB size limit"
        msg = msg.format(obj.filename, obj.sizelimit)
        super().__init__(msg)

class IncorrectFormatError(Exception):
    def __init__(self, filename, reason):
        msg = "Error: file '{}' is incorrectly formatted. {}".format(filename, reason)
        super().__init__(msg)
