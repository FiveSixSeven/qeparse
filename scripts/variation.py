#! /usr/bin/env python

import sys
import os
import timeit
import argparse
import configparser
try:
    filedir = os.path.dirname(os.path.realpath(__file__))
    sys.path.append(os.path.join(filedir, os.pardir))
    import lib
except ImportError as ierr:
    print(ierr)
    sys.exit(1)

_SECTION = 'setup'
_NAMELIST = 'namelist'
_CARD = 'card'

# Executable can be either pw.x, or cp.x
# toteng_mapping = {'PW': lib.runtime.pw_toteng, 'CP': lib.runtime.cp_toteng}

# strip quotes
qstrip = lambda string : string.strip("\"'")


class InputSetup:
    '''
    Read Configuration File and Data Files (iterator)
    '''

    def __init__(self, configfile):

        self.parser = configparser.ConfigParser()
        if not self.parser.read(configfile):
            raise VariationError()

        try:
            self._read_parser()
        except (configparser.NoOptionError, configparser.NoSectionError) as ierr:
            raise VariationError(ierr)

    def _read_parser(self):
        '''
        Read the config file
        - template
        - type_select (namelist or card)
        - type_name (namelist or card name)
        - datafile
        - exepath
        - variable (Namelist only)
        - data_length (Cards only)
        '''
        self.template = qstrip(self.parser.get(_SECTION, 'template'))
        self.type_select = qstrip(self.parser.get(_SECTION, 'type_select'))
        self.type_name = qstrip(self.parser.get(_SECTION, 'type_name'))
        self.datafile = qstrip(self.parser.get(_SECTION, 'datafile'))
        self.exepath = qstrip(self.parser.get(_SECTION, 'exepath'))
        self.variable = None
        self.data_length = None

        # type_select speific selections
        # selection: namelist
        if self.type_select == _NAMELIST:
            self.variable = qstrip(self.parser.get(_SECTION, 'variable'))
        # selection: card
        elif self.type_select == _CARD:
            self.data_length = int(qstrip(self.parser.get(_SECTION, 'data_length')))
        else:
            raise VariationError('unknown value for type_select: {}'.format(self.type_select))

    @property
    def steps(self):
        ''' Return steps iterator, depending on the variation type_select '''
        # selection: namelist
        if self.type_select == _NAMELIST:
            return self._namelist_steps()
        # selection: card
        elif self.type_select == _CARD:
            return self._card_steps()
        else:
            raise VariationError('unknown value for type_select: {}'.format(self.type_select))

    def _namelist_steps(self):
        '''
        Return a generator for all the values of the selected namelist variable
        Yields individual values (One per line)
        '''
        with open(self.datafile, 'r') as fdata:
            while True:
                line = fdata.readline().strip()
                if line:
                    yield line
                else:
                    break

    def _card_steps(self):
        '''
        Return a generator for all the values of the selected card
        Yields lists using self.data_length
        '''
        with open(self.datafile, 'r') as fdata:
            temp = []
            while True:
                line = fdata.readline().strip()
                if line:
                    temp.append(line)
                    if len(temp) == self.data_length:
                        # Yield data list, setupfor next cycle
                        yield temp
                        temp = []
                else:
                    break


class Variation(object):
    '''
        Setup the Varaiotion object

        configfile : Configuration File
        prefix : execution prefix (ex: './', 'mpirun -np 4', ...)
        postfix : execution postfix (command line arguments)
        outfile : Where all
    '''
    def __init__(self, configfile, prefix, postfix, outfile, printall, clean):

        # Config setup object
        self.config = InputSetup(configfile)

        # Main VaratioCalc object
        self.calc = lib.runtime.Calculation(
            inputfile=self.config.template,
            fromfile=True,
            exepath=self.config.exepath,
            prefix=prefix,
            postfix=postfix,
        )

        # Final output file
        self.outfile = outfile

        # Print All Input and output files
        self.printall = printall

        # Clean the directory
        self.clean = clean

        # Set root input/output name
        if self.printall:
            prefix = self.calc.infile.namelist_get('CONTROL', 'prefix')
            if prefix:
                self.printall_root = prefix
            else:
                self.printall_root = self.calc.exetype

        # Number of atoms (needed for energy per atom)
        self.nat = int(self.calc.infile.namelist_get('SYSTEM', 'nat'))

    def _update(self, value):
        ''' Wrapper for update the Calculation '''
        # Update the Calculation object
        # 1) Namelist
        if self.config.type_select == _NAMELIST:
            if not self.config.variable:
                raise VariationError('Name of variable not supplied to VarCalculation')
            self.calc.infile.namelist_set(self.config.type_name, self.config.variable, value)
            return
        # 2) Card
        elif self.config.type_select == _CARD:
            self.infile.card_set(self.config.type_name, value)
        else:
            raise VariationError('unknown value for type_select: {}'.format(self.config.type_select))

    def _run(self, *args, **kwargs):
        '''Wrapper for run from the superclass (May be removed)'''
        self.calc.run(*args, clean=self.clean, **kwargs)

    def variation_loop(self):
        '''Main Variation loop'''
        try:
            self._totals_init()

            # stop+step so that we actually hit the stop
            for index, value in enumerate(self.config.steps):

                # Update the data
                self._update(value)

                # Run Quantum Espresso
                start_time = timeit.default_timer()
                self._run()
                end_time = timeit.default_timer()

                # Update function for the variation loop
                self._totals_update(index, value, end_time - start_time)

                # Printout all files
                if self.printall:
                    self._printall_files(index)

                # Display to the console
                self._display(index, value)

        except lib.runtime.CalcRunError as ierr:
            self._crash_report(ierr)

    def _totals_init(self):
        self.varvals = []
        self.totengs = []
        self.times = []

    def _totals_update(self, index, value, time):
        self.varvals.append(value)
        self.times.append(time)
        if self.calc.outfile.complete:
            self.totengs.append(self.calc.outfile.toteng)
        else:
            self.totengs.append(None)

    def _printall_files(self, index):
        # Printout all files if printall = True
        self.calc.print_files(
            input_name='{}.in{}'.format(self.printall_root, index + 1),
            output_name='{}.out{}'.format(self.printall_root, index + 1)
        )

    def _display(self, index, value):
        # Display to console
        if self.config.type_select == _NAMELIST:
            print('{:4d}) {:<20}'.format(index + 1, value), end='')
        elif self.config.type_select == _CARD:
            print('{:4d}) '.format(index + 1), end='')
        print('     {:12.7f} Ry'.format(self.totengs[-1]), end='')
        print('     {:12.7f} Ry/atom      in {:5.2f} s'.format(self.totengs[-1] / self.nat, self.times[-1]))

    def _crash_report(self, ierr):
        print('\n\n==================================== \n'
              'Calculation Crashed:\n'
              'Check: CRASH.in, CRASH.out, CRASH'
              '\n==================================== \n')
        print(ierr.stdout)
        print(ierr.stderr)
        with open('CRASH.in', 'w') as f:
            print(self.calc.infile.write(), file=f)
        with open('CRASH.out', 'w') as f:
            print(ierr.stdout, ierr.stderr, file=f)
        sys.exit(1)

    def write_data(self):
        # print("\nWriting output: {}".format(self.outfile))

        with open(self.outfile, 'w') as f:
            ncount = 0
            for varval, eng, time in zip(self.varvals, self.totengs, self.times):
                ncount += 1
                if self.config.type_select == _NAMELIST:
                    print('{:7d}  {:20} {:15.7f} {:15.7f} {:15.3f}'.format(ncount,
                                                        varval, eng, eng / self.nat, time), file=f)
                elif self.config.type_select == _CARD:
                    print('{:7d}  {:15.7f} {:15.7f} {:15.3f}'.format(ncount,
                                                        eng, eng / self.nat, time), file=f)
                    print('\n'.join(varval), end='\n\n', file=f)


class VariationError(Exception):
    pass

# def initialize(configfile, prefix, postfix, outfile, clean):
#    '''
#    Setup a Variation Calculation:
#
#        configfile : Configuration File
#        prefix : execution prefix (ex: './', 'mpirun -np 4', ...)
#        postfix : execution postfix (command line arguments)
#        outfile : Where all
#    '''
#
#    # Config setup object
#    config = InputSetup(configfile)
#
#    # Main VarCalculation object
#    calc = VariationCalc(
#        config.type_select,
#        config.type_name,
#        variable=config.variable,
#        inputfile=config.template,
#        fromfile=True,
#        exepath=config.exepath,
#        clean=clean,
#        prefix=prefix,
#        postfix=postfix,
#    )
#
#    # Returns the calc function and the *generator* steps
#    return calc, config.steps
#
#
# def welcome_print(calc, outfile, printall):
#    print("\nQuantum Espresso Variation Study\n")
#    if calc.type_select == _NAMELIST:
#        print("Varying: {}, {}".format(calc.type_name, calc.variable))
#    elif calc.type_select == _CARD:
#        print("Varying: {}".format(calc.type_name))
#    print("Template File: {}".format(calc.infile_name))
#    print("Executable: {}".format(calc.exe))
#    print("Printall: {}".format(printall))
#    print("Clean: {}".format(calc.clean))
#    print("Outputfile: {}".format(outfile))
#
#
# def variation_loop(calc, steps, printall):
#
#    # Number of atoms
#    nat = int(calc.infile.namelist_get('SYSTEM', 'nat'))
#
#    # Set root input/output name if printall = True
#    if printall:
#        prefix = calc.namelist_get('CONTROL', 'prefix')
#        if prefix:
#            file_rootname = prefix
#        else:
#            file_rootname = calc.exetype
#
#    print("\nMain Variation Loop:")
#    try:
#        varvals = []
#        totengs = []
#        times = []
#
#        # stop+step so that we actually hit the stop
#        for index, value in enumerate(steps):
#            varvals.append(value)
#            calc.update(value)
#
#            # Run Quantum Espresso
#            start_time = timeit.default_timer()
#            calc.run()
#            times.append(timeit.default_timer() - start_time)
#            totengs.append(toteng_mapping[calc.exetype](calc))
#
#            # Printout all files if printall = True
#            if printall:
#                calc.print_files(
#                    input_name='{}.in{}'.format(file_rootname, index + 1),
#                    output_name='{}.out{}'.format(file_rootname, index + 1)
#                )
#
#            # Display to console
#            if calc.type_select == _NAMELIST:
#                print('{:4d}) {:<20}'.format(index + 1, value), end='')
#            elif calc.type_select == _CARD:
#                print('{:4d}) '.format(index + 1), end='')
#            print('     {:12.7f} Ry'.format(totengs[-1]), end='')
#            print('     {:12.7f} Ry/atom      in {:5.2f} s'.format(totengs[-1] / nat, times[-1]))
#
#    except lib.runtime.CalcRunError as ierr:
#        print('\n\n==================================== \n'
#              'Calculation Crashed:\n'
#              'Check: CRASH.in, CRASH.out, CRASH'
#              '\n==================================== \n')
#        print(ierr.stdout)
#        print(ierr.stderr)
#        with open('CRASH.in', 'w') as f:
#            print(calc.write(), file=f)
#        with open('CRASH.out', 'w') as f:
#            print(ierr.stdout, ierr.stderr, file=f)
#        sys.exit(1)
#
#    return varvals, totengs, times
#
#
# def write_data(calc, outfile, varvals, totengs, times):
#    print("\nWriting output: {}".format(outfile))
#
#    # Number of atoms
#    nat = int(calc.namelist_get('SYSTEM', 'nat'))
#
#    with open(outfile, 'w') as f:
#        ncount = 0
#        for varval, eng, time in zip(varvals, totengs, times):
#            ncount += 1
#            if calc.type_select == _NAMELIST:
#                print('{:7d}  {:20} {:15.7f} {:15.7f} {:15.3f}'.format(ncount,
#                                                            varval, eng, eng / nat, time), file=f)
#            elif calc.type_select == _CARD:
#                print('{:7d}  {:15.7f} {:15.7f} {:15.3f}'.format(ncount,
#                                                                    eng, eng / nat, time), file=f)
#                print('\n'.join(varval), end='\n\n', file=f)
#
#
def parse_argv(argv):

    welcome = "Quantum Espresso Lattice Constant Study for Graphene"
    parser = argparse.ArgumentParser(
        # usage='%(prog)s [options] positional',
        description=welcome,
        formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument(
        'configfile',
        action='store',
        type=str,
        help='Configuration file with: \n[setup]'
             '\ntemplate = <template_file>'
    )
    parser.add_argument(
        '--outfile',
        action='store',
        type=str,
        default='variation.dat',
        help='File for energy and variation data'
    )
    parser.add_argument(
        '--prefix',
        action='store',
        type=str,
        default='',
        help='String to precede the executable (example: \'mpirun -np 4\')'
    )
    parser.add_argument(
        '--postfix',
        action='store',
        type=str,
        default='',
        help='Command line arguments to the executable'
    )
    parser.add_argument(
        '--clean',
        action='store_true',
        default=False,
        help='Clean working directory of all generated files'
    )
    parser.add_argument(
        '--printall',
        action='store_true',
        default=False,
        help='Print all input and output files as prefix.in<NUM>/prefix.out<NUM>, set clean = False'
    )
    print()
    args = parser.parse_args()

    if args.printall:
        args.clean = False

    return args
#
#
# if __name__ == '__main__':
#    args = parse_argv(sys.argv[1:])
#    calc, steps = initialize(args.configfile, args.prefix, args.postfix, args.outfile, args.clean)
#    welcome_print(calc, args.outfile, args.printall)
#    varvals, totengs, times = variation_loop(calc, steps, args.printall)
#    write_data(calc, args.outfile, varvals, totengs, times)
